De Gautheron 
Quant Gauteron se marïa,
Marïon prist, qui dit li a
que l’aime molt et est pucele. 
La nuit jurent et cil et cele
en .i. lit souz une cortine.
Gautier prent Marïon sovine. 
Son vit au con li apoucha
et Marïon .i. poi guicha,
et Gautier s’afiche, si boute. 
Par pou n’i met la coille tote 
et si roidement l’asailli
c’un grant pet du cul li sailli.
Quant il oï le pet qui saut,
« Dame, dist il, se Dex me saut, 
je sai bien et si ai senti 
que de covent m’avez menti 
car pucele n’estïez pas ! » 
El i respont enelepas :
« Jel fui, mes je nel sui or mie, 
et vos fetes grant vilenie 
et si me dites grant outrage.
N’oïtes vos le pucelage, 
qui s’en fuï quant vos boutastes ? 
Molt vileinement l’en chaçastes !
- Par le cuer Dieu, fet il, il put ! 
Ce poise moi que il se mut ! 
Bien fust eul con a une part 
car g’en eüsse asez du cart ! 
Bien i peüst estre en son lieu ! » 
Por ce maudi ge que de Dieu
soit la pucele confondue, 
qui tant le garde que il pue ! 

Explicit 
