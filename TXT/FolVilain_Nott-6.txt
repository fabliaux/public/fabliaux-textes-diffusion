Del fol vilain 
Puis qu’il vient a vos tos a bel, 
dire me covient .i. fablel
qui n’est de contes ne de rois,
de garnemens ne de conrois,
mais d’un grant vilain malostrut.
Non eut Evrars, li fils Tietrut. 
Cille Tietrus fu tote lorde
et si refu et borgne et sorde.
Ses barons ot non Gonderrés.
Onques ne fu tondus ne res.
Gros eut le cief, les ceviax ros,
et nes de cat et hure d’ors.
Evrars, ses fius, fu ansi fais. 
De se matere et de ses fais
vos volrai ja un poi retraire, 
si com il volt sen ombre traire 
la u s’aleve deduissant
tos seus a le lune luissant,
si porteve sen arc tendut. 
Tant eut a l’aler entendut 
qu’il vit de lui le forme vainne
selonc une moie d’avainne, 
se cuida que ce fust Robués
qui li volist embler ses bués.
Qant li vilains sen ombre voit, 
d’une grant geule qu’il avoit
a escriet : « Or cha, baron ! 
Ves ci Robuedin le laron ! » 
Tant a criet et bas et haut 
qu’il i vinrent li fil Mehaut : 
c’est Hellinés et Godefrois
qui gardevent lor palefrois.
Qant li vilains vit le secors,
encontre aus est venus le cors, 
si lor a dit comme musars : 
« Pensés que Robuedins soit ars ! 
Il doit bien estre a mort juciés ! 
Il est en ces garbes muciés ! » 
Li vilains par ahatisson
a envoiet por un tisson,
s’a en trois lius le moie esprisse,
puis a une grant maque prisse, 
si va entor le feu criant :
« Lere, que vas te detriant ? 
Car huce Dex consumatus 
ains que tu soies abatus ! » 
Mais li leres n’en savoit mot 
qu’il li rovast dire cel mot
car il estoit a Walecort, 
a l’ostel Englebert le cort
qui maint laron ot sostoitiet. 
Tant a Evrars le feu coitiet
qu’il n’i remest palle ne grains, 
mais puis en fu dolans et grains 
qu’il li covint le moie sorre 
le segnor Baudouïn de Sorre 
qui l’avainne avoit fait soier
et amasser et amoier. 
Puis li ravint en es le mois
qu’il vendi .i. mui de tremois.
Qant il eut les deniers recius, 
il a qatre borjois percius 
qui bevoient molt durement
claret a çucre et a piument.
Li vilains est cele part trais.
Qant il lor voit boire grans trais, 
si lor a dit par grant dangier : 
« Poroie joste vos mangier ? » 
Cil virent le vilain redois 
as grandes mains et as lais dois
et a le cape corte et viés.
« Amis, fait il, nos bevons miés.
Or venés sapiier qu’il set,
puis nos en dites vo penset. » 
Atant li puirent le hanap
et cil li a donet un lap.
Encor fust il grans et parfons, 
si le but il desci qu’el fons ! 
Puis dist : « Bien ait qui te brasa 
et qui premiers te porpensa ! »
Puis fait mander des blans gasteax, 
des flaans et des canestiax
et un grant maquerel salet,
et si but trois los de claret,
mais tant i ot a apaier,
qant ce vint a l’escot paier,
qu’il i covint venir Guinant,
qui provos estoit de Dinant, 
qui li fist laier sen sorcot
por .iiii. sous de sen escot. 
Encor fist il autre folie
car farine li ert falie,
si boli sen lait a le cauç. 
Cil qui abevrerent le sauç 
estoient si cousin germain.
Il meïsmes i mist sa main, 
si l’aida tant a abascier
qu’il le fist en l’eve plascier. 
Puis fist a unes rovoisons 
escorcier trestos ses oisons 
poruec c’on li eut dit en fable
qu’il devenroient trestos sable. 
Encor fist il une lordure
qui molt li fu et aspre et dure, 
qu’i li avint par un demars 
qu’il enfoï plus de .vii. mars
joste un vivier en une arainne.
D’autre part crioit une rainne,
si disoit .viii., c’estoit avis,
et li vilains enrajoit vis.
« Pute, fait il, vos i mentés. 
Jes ai plus de .vii. fois contés ! »
Entrués qu’il crioient ensanle,
uns escuiers illuec s’asanle.
« Diva, fait il, q’as tẹ a tencier 
et ne sai cui a manecier ?
- Sire, fait il, cille ranuce,
qui en cele eve saut et muce,
dist que j’ai chi .viii. mars repus, 
mais no geline n’a puis pus
que jes contai, par saint Remi ! 
N’en i a que .vii. et demi !
Cil gissent desos cele piere, 
mais nus n’en puet savoir espiere
fors le ranuce qui le vit. 
Ce poisse moi qu’ele tant vit ! » 
Li escuiers respont briement :
« Va por .i. fust isnelement,
si l’ocirai, se Dex me voie ! »
Atant se met cil a la voie,
si vait el bos un fust cuellir,
et cil vait l’argent recuellir.
Encor fist il une mervelle, 
qu’il s’en aleve a une velle
tos seus parmi une vert pree.
Molt estoit bele l’avespree.
Il ascouta aval les cans, 
s’oï des davodiaus les cans
qui s’en alevent devant lui. 
Robins i estoit de Fellui 
et Gautelos et Roimondins
et li fius Godefroit Bondins. 
Qant cil ses compagnons entent,
plus n’i areste ne atent. 
Tant a alet plus que le pas
qu’il est venus a un trespas,
en une cave joste .i. mont,
si escria : « Roimont, Roimont,
atendés me, je vois, je vois ! » 
Li mons ens el fin de le vois 
retentissoit si durement 
qu’il cuida bien visablement 
que ce fust Roimondins li clos
qui en le roce fust enclos. 
Il li escrie de recief :
« Mar i muçastes, par men cief ! 
Ça fors vos covenra sallir ! »
Adont vait le roce asallir,
s’i a plus de .c. cols getés, 
mais il i ot asés de tes 
qui molt li fissent grant anui
car il repairoient sor lui. 
Tant i geta que de ses cols 
li fu trestos sanglens li cols
et li vïaires tos vermaus, 
mais puis en dut estre grans maus 
car il se plainst a se parage 
que par orguel et par oltrage 
l’avoient il qatre asalit
com recreant et com falit,
mais li voisin en dissent tant,
qui le troverent seul getant,
que cil en furent descoupé. 
Cil Evrars fu parens Coupé
dont vos avés oï pieça,
qui le messe recommença.
Ç’avint Evrart, et plus assés,
car ains que li ans fust passés, 
li aplaidierent si parent 
qui conversoient la par ent
une pucele sans avoir, 
mais molt estoit de grant savoir
et de beauté molt renomee. 
Uns escuiers l’avoit amee
sans vilonie et sans hontage. 
Li vilains por sen iretage
l’eut asés plus que por son sons . 
Li pucele ot a non Mainsens
et ses amis ot non Robers,
qui ne restoit mie bobers, 
ains est a s’amie venus.
Molt simplement s’est maintenus, 
se li a dit a morne ciere :
« Ma douce suer, m’amie ciere,
se cis sos a vo pucelage, 
j’en averai el cors le rage ! 
Vos m’avés grant pieça promis
qu’il l’averoit, vos dols amis ! » 
Cele respont : « S’il pooit estre 
que vos fusciés le nuit en l’estre
pres de le cambre en .i. escons, 
delivrés vos seroit li cons ! »
Et cil respont : « Ma douce amie, 
por tant ne remanra il mie ! » 
Atant s’en va li damoiseaus
deduire as ciens et as oiseaus,
et cele, qui asés savoit,
en une la qu’elë avoit,
que ses amis li ot tramise,
a une grande soris mise,
s’i le garda tros q’al termine,
si c’onques n’i senti famine,
que cil a fait sen asanlee.
Ermenfrois i vint barbe lee, 
et Gonduïns et Godebers 
et Warenbaus et Warenbers 
et des autres vilains .cc.
qui ne flairoient mie encens, 
si ot .xl. davodiaus
a flahutes et a festiaus. 
La veïsciés en mi l’estree 
tante jument enquevestree ! 
Un dïemence par matin 
a le capele saint Martin 
fu esposee Maiselos
qui de beauté avoit le los. 
Qant li services fu finés 
a le vile o Evrars fu nes
en a portee se mollier. 
Qui la veïst sopes mollier 
en une caudiere bolant 
o avoit car de truie olant 
c’on diut le nuit mangier al poivre !
Si fist on savor de genoivre.
Le nuit fu li vilains molt baus.
Asés i ot tresces et baus, 
mais cele ot tres bien et entent
que ses amis lafors l’atent, 
si a son baron apelet, 
se li a bien dit et malet 
qu’ens en le cambre ne remagne
feme qui en le vile magne, 
et cil qui fu de fol cuidier
lor a fait le cambre widier,
puis a fremé l’uis par dedens.
Aprés a eskignié les dens.
Vers celi vient tos eslasciés, 
puis dist : « Vos gambes eslassiés ! 
Je vos volrai mateculer ! »
Cele commence a reculer,
si fist .i. poi le plorivet, 
puis dist : « Ostés vo borlivet ! 
Vos ne l’ariés en moi u metre 
ne por doner ne por prometre ! 
J’ai men con en maison lasciet
en le huge dalés l’asciet. 
La le lasca je senmedi 
en droite eure de mïedi 
car je cremoie le qascier
a l’aler et au cevalcier.
Se vos le voliés raporter,
si nos poriemes deporter. 
Je n’i os autrui envoier
car je criem molt le desvoier. 
Ne volroie por nule rien 
nus i adesast de sen rien 
car il me seroit reprovet ! » 
Or entendés del fol provet 
comment se feme le desvoie 
qui por sen con querre l’envoie 
ensus de li une grant liue !
Li vilains monte sor sen iue. 
Ainc ne fina desci q’al mes
u li cons dut estre remés.
Le laa prent, si s’en retorne,
et cele d’autre part s’atorne, 
si est a sen ami venue
en sa cemisse tote nue, 
et cil qui molt l’eut desiree
ne li fist mie ciere iree,
ains l’a entre ses bras saisie,
si l’a molt dolcement baisie, 
et se li fist plus de .iii. fois 
sans maltalent et sans defois ! 
Or rediromes del vilain 
qui tant a qoitié Morain 
qu’il a trovet ens en un val
un gues c’on passoit a ceval. 
La a se jument abevree
comqu’il avoit molt le nuit grevee, 
et li soris pas ne reposse
qui en le la estoit enclose.
Cil ot le soris randoner. 
Li vis li prent a brandoner !
« Certes, fait il, ce m’est avis
que por cest con m’esta li vis.
Je le veroie volentiers,
savoir s’il est sains et entiers, 
car j’ai maintes fois oï dire 
nus ne vit onques con entire
qu’il ne fust fendus o traués.
Mais ains que g’isse de cest wes, 
sara je comfait li con sont 
et comfaites cieres il font ! »
Puis a le laa descoverte,
si l’a trestote en ample overte. 
A icest mot li soris saut
tantost com li laa li faut, 
s’est volee tote sovine
en mi le fil de le ravine, 
mais li nuis estoit si oscure 
c’on ne peut coisir le figure
de le soris ne le sanlance. 
Adont li vint en ramembrance
de Damerdeu a preecier. 
Adont commença a hucier :
« Sains Gilles, je vos requerrai 
se je le con me feme rai ! » 
Puis est ens en l’eve salis
si que li piés li est falis. 
Tant quist le con de totes pars 
que li parlemens fu espars
de celi et de son ami. 
Or entendés .i. poi a mi 
Jevos dirai qu’il en avintunvilain quirices devint
I comment le baceler avint.
Qant il ariere s’en revint,
il est venus au gues errant,
si cuida abevrer Ferrant, 
si a veüt oscurement
le vilain joste se jument. 
Il li a dit : « Sont ce nuitun 
qui la peskent en cel betun ? 
- Nenil, amis, ains sui uns hom
qui bien commenceroit a son. 
Je sui trestos li plus dolens 
qui soit dementres q’a Meulens ! 
J’avoie une pucele prisse
preut et cortoise et bien aprisse,
s’avoit ça sen con commandet.
Or l’avoit par moi remandet, 
si l’ai en ceste eve perdut ! » 
Dont a li vallés respondut :
« Amis, fait il, se Dex me voie,
je l’encontrai en ceste voie. 
Il puet ja estre a te maison ! »
Qant cil entendi le raison, 
il est sallis de l’eve fors
car molt estoit vistes et fors,
puis vait criant : « Men con, men con ! » 
Or entendés del fol bricon ! 
Ainc cil cris ne pueut remanoir 
desci qu’il vint a son manoir !
Se feme vait au piet caïr, 
puis dist : « Molt me poés haïr 
car j’ai perdut no Conebert
el gues Martin, le fil Herbert. »
Cele respont : « Sire, jel rai !
- Certes, fait il, ja nel crerai
se je nel sent tot nut a nut. »
Atant sont ensanle venut. 
Cele li a tant consentit
qu’il a le con de plain sentit,
mais il ot esté en estor,
s’estoit .i. poi sullens entor. 
Dist li vilains : « Il est molliés 
et del betun encor solliés !
Par ço sa ge bien que c’est il.
Or laiés resüer l’ostil,
si ferai ço que faire doi. »
Atant s’en vont dormir andoi.
Gautiers Li Leus atant le lait.
Le conte del fol vilain lait. 
De qanque il fisent puiscedi
je n’en sai plus ne plus n’en di. 

