Ci commence le dit des .iii. dames de Paris 
adis souloient les merveilles 
conter as festes et as veilles
Colins, Hauvis, Jetrus, Hersens. 
Or sont a Paris de touz sens 
les maisons plaines et les rues 
de grans merveilles avenues
a .iii. fames nouvelement, 
si com vous l’orrez ja briement
se de vous puis estre escoutez. 
Haus jours iert et sollempnitez
c’on dit des .iii. rois de Couloigne. 
Conter ne vous i veul mençoigne
fors que droite verité pure, 
mais onques sifaite aventure 
en pays du monde n’avint
l’an c’on dit .m. ccc. et vint 
.i. mardi devant la grant messe 
que la fame Adam de Gonnesse 
et sa niece Maroie Clippe 
distrent que chascune a la trippe
iroient .ii. deniers despendre, 
s’ en alerent sanz plus atendre 
entre elles .ii. a la taverne 
en la maison Perrin du Terne
qui nouviaus taverniers estoit. 
Si com l’une l’autre hastoit
qu’elles vouloient ens entrer, 
lors revint droit a l’encontrer 
dame Tifaigne la coifiere 
qui dist : « Je sai vin de riviere
si bon qu’ainz tiex ne fu plantez.
Qui en boit, c’est droite santez
car c’est uns vins clers, fremïans,
fors, fins, fres, sus langue frians,
douz et plaisanz a l’avaler.
A celui nous couvient aler, 
autre vin goust ne nous ara 
ne ja hons ne nous i sara
pour demorer .iii. jours entiers, 
et si nous croira volentiers
li ostes chascune .x. souls. 
- Ses cors soit benis et absouls 
de celle qui si bien parla !
dist Margue. Alons celle part la ! 
Il i fait bon, se Diex m’avoie ! » 
A tant se metent a la voie
vers la taverne des Maillez.
La vint le filz Druïns Baillez, 
uns varlés qui vint avec eles
par cui soi toutes leur nouveles. 
Cis les servi a leur mengier 
et leur aporta sanz dangier
quan c’on pot de bon recouvrer. 
La veïssiez des denz ouvrer 
et henas emplir et widier ! 
En petit d’eure a mon cuidier
orent .xv. sous despendu. 
« Riens ne m’ara savour rendu
a cest mengier, dist Margue Clouue, 
se nous n’avons d’une crasse oue
et des aus plainë une escuele. »

Lors court Druïns par la ruele
en l’ostel ou on les cuisoit.
.II. en prist, et aprés puisoit 
des aus tout plain .i. grant platel 
et a chascune .i. chaut gastel
aporta quanqu’il pot haster. 
Qui veïst chascune taster 
ces fors aus et celle oue crasse ! 
Mengié l’orent en mains d’espasse 
assez c’on ne mist au tuer ! 
Lors commença Margue a suer 
et boire a grandes henapees. 
En poi d’eure erent eschapees
.iii. chopines par mi sa gorge.

« Dame, foi que je doi saint Jorge,
dist Maro Clippe sa commere, 
cis vins me fait la bouche amere ! 
Je veul avoir de la garnache !
Se vendre devoie ma vache, 
s’en aurai je au mains plain pot ! » 
Druïn hucha quanqu’elle pot 
et li dist : « Va nous aporter 
pour nos testes reconforter 
de la garnache .iii. chopines
et de tost revenir ne fines,
s’aporte gauffres et oublees,
fromage et amandes pelees,
poires, espices et des nois, 
tant pour florins et gros tornois 
que nous en aions a plenté ! »
Cilz i court, et elle a chanté 
par mignotise .i. chant nouvel :
« Commere, menons bon revel ! 
Tiex vilains l’escot paiera 
qui ja du vin n’ensaiera ! »

Ainssi chascune se deporte, 
et Druïns le fort vin aporte
qui fu par les henas versez.
« Commere, or en bevons assez, 
dist Maroie et dame Fresens, 
car c’est vins pour garder le sens 
mieudres assez que li françois ! »
Lors but chascune, mais ançois 
c’on eüst tornees ses mains 
c’une plus que li autre mains
fu touz lapez et engloutis.
« Cis pochonnez sont trop petis,
dist Maroie, par saint Vincent ! 
Pour boivre le quartier d’un cent
ne nous en couvient esmaier. 
Je ne l’ai fait el qu’essaier ! 
Tant est bons que j’en veul encore !
Or va donc, se Diex te secore,
Druïns, raportes en .iii. quartes, 
car avant que de ci departes 
seront butes ! » Et cis i court
qui tost revint a terme court.

Puis donna son pot a chascune.
« Compains bien veignant, dist li une,
menjë .i. morsel, puis si bois ! 
Cilz vins est mieudres que d’Ervois 
ne que vins de saint Melïon !
- Voire assez ! ce dist Marïon.
Je le boif trop plus volentiers.
Se me pos iert plains touz entiers,
n’en y ara assez tost goute. 
- Hé ! Que tu as la gorge gloute 
dist Maro Clippe, bele niece ! 
Je n’aurai encor en grant piece
but tout le mien, mais tout a trait 
le buverai a petit trait
pour plus sus la langue croupir. 
Entre .ii. boires .i. soupir
i doit on faire seulement, 
si en dure plus longuement
la douceur en bouche et la force. »

En tel point chascune s’esforce 
de garnache engloutre et tant boire 
qu’il n’est nus hons qui peüst croire
comment chascune s’atourna. 
Du matin que il ajourna 
furent la jusqu’a mïenuit 
et menerent si bon deduit
qu’adés orent le henap plain.
« Je veul aler la hors au plain,
dist Margue Clippe, en mi la voie
treschier si que nus ne nous voie, 
si en vaudra trop miex la feste ! 
Chascune aura nue la teste
et s’irons em pures les cors.
- Dont lairés ci vos vardecors,
dist Druïns, de gage a l’escot. 
- S’averez en guise d’escot 
escourchïe pelice et cote
et chemise qu’elle ne crote, 
s’irons treschier parmi la rue !

A tant chascune a terre rue
son corset et son chaperon. 
Escourchié furent li geron
des cotes desus la pelice, 
et Druïns hors de l’uis les glice 
chantant chascune a haute vois :
« Amours, au vireli m’en vois ! »
Mout parloient de leur amis. 
Ainssi son cors chascune a mis
hors a la bisë et au vent, 
si tresbuschoient plus souvent
c’on ne peüst sa main tourner. 
A .ii. lieues pres d’ajourner 
les a Druïns en tel point mises
que cotes, pliçons et chemises,
chaucemente, bourse et corroie
leur toli tout. Je qu’en diroie ? 
Ainssi les lessa toutes nues 
gisanz au fuer des bestes mues
vilment et en divers couvine,
l’une adenz et l’autre souvine,
trebuschies en .ii. monciaus,
plus emboees que pourciaus. 
Tout en tel point Druïns les lait 
ou boier plus grant et plus lait
qui fust en toute la cité. 
La jurent a molt grant vilté 
l’une sus l’autre comme mortes 
tant que par tout guichez et portes 
de la cité furent ouvertes
c’on vit les merveilles apertes. 
Chascuns y acourt pour veoir 
car n’avoient sens ne pooir
d’eles tant ne quant remuer,
qui la les vousist partuer.
Pour mortes les tenoient toutes. 
Testes et mains avoient routes
et touz sanglens cors et visages. 
Touz disoient et fols et sages
c’on les avoit la nuit murdries, 
s’en erent la gent abaubies
du lait point ou il les veoient.

Et leur chetis barons cuidoient 
qu’il fussent em pelerinage ! 
Quant uns preudons de leur visnage 
vint la qui bien les reconut
au cors que chascune ot tout nut,
si le corut leur barons dire, 
qui pasmerent de duel et d’ire 
quant il ont leur fames trouvees 
gisant nues et desrobees
comme merdes en mi la voie. 
N’est hons s’il veult qui ne les voie
par tout et en coste et en mi. 
Lors crierent : « Hareu ! Ainmi ! » 
Et mont tendrement vont plorant.
Ainssi qu’il vindrent la corant,
leur .iii. fames ont reconutes,
qui tant ne quant ne se sont mutes,
gisans nues a tel disfame. 
Les cuers de courouz leur enflame
car cus et testes leur paroit. 
Nus hons raconter ne saroit
qu’eles erent a grant meschief,
n’onques ne murent pié ne chief, 
se furent au moustier portees
des Innocens et enterrees,
l’une sus l’autre toutes vives. 
Hors leur sailloit par les gencives 
li vins et par touz les conduis ! 
Ainçois fu plus de mïenuis 
que se peüssent resveillier 
et mont les convint travaillier 
ainçois qu’elles fussent issues 
hors de la terre et des issues
et des portes des Innocens. 
Elles n’odoient pas encens ! 
Mont erent ordes et puans 
si com gens povres ou truans
qui se couchent par ces ruelles, 
s’en raloient ces .iii. entr’elles
qu’a paines pooient parler. 
Ne poïssent mië aler 
.ii. pas ou .iii. sanz trebuschier. 
Souvent les oïssiez huchier : 
« Druïn ! Druïn ! Ou es alez ? 
Aporte .iii. harens salez 
et .i. pot de vin du plus fort
pour faire a nos testes confort, 
et penses de tost revenir 
pour nous compaignie tenir ! 
Et si clorras la grant fenestre ! » 
Ainssi qu’elles cuidoient estre
en la taverne toutes trois, 
les aqueult uns vens si destrois 
et si frois qu’il les fist pasmer 
et toutes pour mortes clamer
et jus trebuschier en la place.
N’orent bouche, oil ne nes ne face 
qui ne fust de boe couvers
et toutes chargies de vers. 
N’onques ne murent pié ne main 
de ci au jour a lendemain 
que li aube esclarcist et point 
c’on les retrouva en tel point
comme ot fait le jour de devant. 
Droit ainssi qu’a soleil levant
chascuns qui miex miex y acourt, 
mais assez en brief terme et court 
si bien la chose ala et vint 
que cil meïsmes i sourvint 
que le soir les out enterrees.
Et quant ilec les a trouvees, 
de grans marveilles s’en seigna 
et dist : « Dyables les engigna 
qui les a raportees ci !
Oiés, seigneur, pour Dieu merci, 
comment sont elles revenues ? » 
En terre les mist toutes nues 
l’une seur l’autre en une fosse !
« Foi que je doi au cors saint Josse, 
elles ont les deables es cors ! 
Voiés les a chascun des cors
comme elles sont de vers chargies,
enterrees et demengies, 
les cors noirs et delapidés ! 
C’est de les veoir grant pitez ! 
Touz li cuers du ventre m’en tremble ! » 
Ainssi qu’il parloient ensemble
de l’aventure desguisee, 
s’est dame Tifaigne escriee 
qui revint .i. poi en mimoire : 
« Druïn ! Raportez nous a boire !
- Et moi aussi ! dist Maro Clipe. 
Je veul de la nouvele tripe ! »
Ainssi sont relevees toutes.
Dessivres, feles et estoutes
s’en va chascune a son refuit, 
et chascuns de paour s’en fuit 
qui cuident ce soient mauffez 
car les cuers orent eschauffez 
de corrouz quant sont aperçutes 
qu’ainssi orent esté deçutes
et menees par reverie. 
Or pri a chascun qu’il en die 
verité s’onques aventure
oï mais tele en escripture,
et tantost c’on le m’ara dit,
j’en finerai a tant mon dit. 


Explicit le dit des .iii. dames de Paris 
