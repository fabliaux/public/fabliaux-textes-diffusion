Ci conmance de la grue 
Des or que que j’aie targié,
puis qu’il m’a esté enchargié, 
voudré .i. fabliau ici fere 
dom la matiere oï retrere
a Vercelai devant les changes. 
Cil ne sert mie de losenges
qui la m’a racontee et dite.
Ele est et brievë et petite, 
mais or oie qui oïr vialt !
Ce dit Garins, qui dire sialt, 
que jadis fu .i. chastelains
qui ne fu ne fox ne vilains,
ainz ert cortois et bien apris. 
Une fille avoit de haut pris
qui estoit bele a desmesure, 
mes li chastelains n’avoit cure 
qu’en la veïst se petit non
ne que a li parlast nus hon. 
Tant l’avoit chiere et tant l’amoit 
que en une tor l’enfermoit. 
N’avoit o li que sa norrice
qui n’estoit ne fole ne nice,
ainz ert molt sagẹs et molt savoit.
La pucele gardee avoit. 
Molt l’avoit bien endotrinee ! 
.I. jor par une matinee 
vost la norice aparellier
a la damoisele a mengier,
si li failli une escuelle. 
Tot maintenant s’en corut cele
a lor ostel, qui n’est pas loing,
querre ce dont avoit besoing.
L’uis de la tor overt laissa. 
A tant .i. vaslet trespassa
par devant la tor, qui portoit
une grue que prise avoit,
si la tenoit en sa main destre.
La pucele ert a la fenestre.
A l’esgarder hors se deporte. 
Le vaslet qui la grue porte
apela, si li dist : « Biax frere, 
or me di par l’arme ton pere 
quel oisel est ce que tu tiens !
- Dame, par toz les sains d’Orliens,
c’est une grue grant et bele.
- En non Dieu, fet la damoisele, 
ele est molt granz et parcreüe ! 
Ainz tele mes ne fu veüe ! 
Je l’achetasse ja de toi ! 
- Dame, fet li vaslez, par foi,
s’il vos plest, je la vos vendré.
- Or di donc que je t’en donré.
- Dame, por .i. foutre soit vostre.
- Foi que doi saint Pere l’apostre, 
je n’ai nul foutre por changier !
Ja ne t’en feïsse dangier,
se l’eüsses, se Diex me voie. 
Tantost fust ja la grue moie !
- Dame, fait il, ice est gas ! 
Ice ne querroie je pas
que de foutre a plenté n’aiez.
Mes fetes tost, si me paiez ! »
El jure, se Diex li aït,
c’onques encor foutre ne vit.
« Vaslez, fet ele, vien amont, 
si quier et aval et amont !
Soz bans, soz lit partot querras, 
savoir se foutre i troveras ! »
Li vaslez fu assez cortois.
En la tor monta demenois,
sanblant fet de querre partot.
« Dame, fet il, je me redot
qu’il ne soit soz vostre pelice. » 
Cele qui fu et sote et nice
li dist : « Vaslez, vien si i garde ! »
Et li vaslez plus ne s’i tarde.
La damoisele a enbraciee,
qui de la grue estoit molt liee.
Sor lou lit l’a cochiee et mise, 
puis li solieve la chemise.
Les james li leva en haut.
Au con trover mie ne faut,
lo vit i bote roidement.
« Vaslez, tu quiers trop durement !
fet la pucele qui sospire.
Li vaslez conmença a rrire,
qui est espris de la besoingne.
« Drois est, fet il, que je vos doingne 
ma grue ; soit vostre tot quite !
- Tu as bone parole dite,
fet la meschinë. Or t’en torne ! »
Cil la lessa pensive et morne,
si s’en issi de la tor fors.
Et la norice i entra lors,
si a aparceü la grue. 
Toz li sans li fremist et mue ! 
Lors a parlé tost et isnel : 
« Qui aporta ci cest oisel ?
Damoisele, dites lou moi ! 
- Je l’achetai or par ma foi ! 
Je l’ai d’un vaslet achetee
qui caienz la m’a aportee.
- Qu’i donastes ? - .I. foutre, dame. 
Il n’en ot plus de moi par m’ame !
- .I. foutre ? Lasse, dolerouse, 
or sui je trop maleürouse 
quant je vos ai leissiee sole ! 
.C. dahaiz ait mauvese gole 
quant onques menjé en ma vie ! 
Or ai ge bien mort deservie,
et je l’avré, ge cuit, par tens ! » 
Par pou n’est issue do sens
la norrice et chiet jus pasmee.
Quant se relieve, s’a plumee
la grue et bien aparelliee.
Ja n’i avra, ce dit, ailliee,
ainz en voudra mengier au poivre. 
« Sovent ai oï amentoivre 
et dire et conter en maint leu : 
li domages qui bout au feu 
vaut miax que cil qui ne fet aise ! »
Qui que soit bel ne qui desplaise,
la grue atorne bien et bel, 
puis si reva querre .i. cotel
dom ele vialt ovrir la grue. 
Et la meschine est revenue
a la fenestre regarder,
si vit lou vaslet trespasser,
qui molt est liez de s’aventure. 
Et la damoisele a droiture
li dist : « Vaslez, venez tost ça ! 
Ma norrice se correça 
de ce que mon foutre enportastes
et vostre grue me laissastes.
Par amor, venez lou moi rendre !
Ne devez pas vers moi mesprendre.
Venez, si fetes pes a moi !
- Ma damoisele, je l’otroi ! » 
fet li vaslés. Lors monte sus. 
La damoisele giete jus
et entre les janbes li entre,
si li enbat lou foutre el ventre.
Quant ot fet, tantost s’en ala,
mes la grue pas n’i laissa,
ainz l’en a avec soi portee. 
Et la norice est retornee
qui sa grue vialt enhaster.
« Dame, ne vos estuet haster,
fet la meschine, qu’i l’en porte,
qui s’en est issuz par la porte.
Desfoutue m’a, jel vos di ! »
Quant la norice l’entendi,
lors se debat, lors se devore, 
et dit : « Que maudite soit l’ore 
que je onques de vos fui garde ! 
Trop en ai fet mauvese garde 
quant si avez esté foutue 
et si n’ai mie de la grue ! 
Je meïsmes li ai fet leu ! 
La male garde pest lo leu ! » 

