C’est li testament de l’asne 
Qui wet au siecle a honeur vivre 
et la vie de seux ensuyre
qui beent a avoir chevance,
mout trueve au siecle de nuisance, 
qu’il at mesdizans d’avantage 
qui de ligier li font damage,
et si est touz plains d’envïeux,
ja n’iert tant biaux ne gracïeux.
Se dix en sunt chiez lui assis, 
des mesdizans i avra six
et d’envieux i avra nuef. 
Par derrier ne prisent .i. oes 
et par devant li font teil feste
chacuns l’encline de la teste. 
Coument n’avront de lui envie
cil qui n’amandent de sa vie,
quant cil l’ont qui sont de sa table, 
qui ne li sont ferm ne metable ?
Ce ne puet estre, c’est la voire ! 
Je le vos di por .i. prouvoire
qui avoit une bone esglise, 
si ot toute s’entente mise
a lui chevir et faire avoir.
A ce ot tornei son savoir.
Asseiz ot robes et deniers,
et de bleif toz plains ces greniers, 
que li prestres savoit bien vendre 
et pour la venduë atendre 
de Paques a la Saint Remi, 
et si n’eüst si boen ami 
qui en peüst riens nee traire
s’om ne li fait a force faire.

Un asne avoit en sa maison 
- mais teil asne ne vit mais hom -
qui vint ans entiers le servi,
mais ne sai s’onques teil serf vi.
Li asnes morut de viellesce,
qui mout aida a la richesce. 
Tant tint li prestres son cors chier 
c’onques nou laissat acorchier
et l’enfoÿ ou semetiere. 
Ici lairai cestematiere.

L’evesques ert d’autre maniere
que covoiteux ne eschars n’iere,
mais cortois et bien afaitiez, 
que c’il fust jai bien deshaitiez
et veïst preudome venir,
nuns nel peüst el list tenir. 
Compeigniẹ de boens crestïens
estoit ces droiz fisicïens.
Touz jors estoit plainne sa sale.
Sa maignie n’estoit pas male,
mais quanque li sires voloit,
nuns de ces sers ne s’en doloit.
C’il ot mueble, ce fut de dete
car qui trop despent, il s’endete.

Un jour grant compaignie avoit
li preudons qui toz biens savoit, 
si parla l’en de ces clers riches 
et des prestres avers et chiches 
qui ne font bontei ne honour
a evesque ne a seignour. 
Cil prestres i fut emputeiz
qui tant fut riches et monteiz. 
Ausi bien fut sa vie dite
con c’il la veïssent escrite, 
et li dona l’en plus d’avoir
que troi n’em peüssent avoir, 
car hom dit trop plus de la choze
que hom n’i trueve a la parcloze.

« Ancor at il teil choze faite 
dont granz monoie seroit traite
s’estoit qui la meïst avant,
fait cil qui wet servir devant,
et c’en devroit grant guerredon.
- Et qu’a il fait ? dit li preudom.
- Il at pis fait c’un Bedüyn, 
qu’il at son asne Baudüyn
mis en la terre beneoite.
- Sa vie soit la maleoite,
fait l’esvesques, se ce est voirs ! 
Honiz soit il et ces avoirs !
Gautier, faites le nos semondre, 
si orrons le prestre respondre
a ce que Robers li mest seure.
Et je di, se Dex me secoure,
se c’est voirs, j’en avrai l’amende ! 
- Je vos otroi que l’an me pande 
se ce n’est voirs que j’ai contei ! 
Si ne vos fist onques bontei ! »

II fut semons. Li prestres vient.
Venuz est, respondre couvient 
a son evesque de cest quas
dont li prestres doit estre quas.
« Faus desleaux, Deu anemis, 
ou aveiz vos vostre asne mis ?
dist l’esvesques. Mout aveiz fait 
a sainte Esglise grant meffait 
- onques mais nuns si grant n’oÿ - 
qui aveiz votre asne enfoÿ 
la ou on met gent crestïenne !
Par Marie l’Egyptïenne, 
c’il puet estre choze provee
ne par la bone gent trovee,
je vos ferai metre en prison, 
c’onques n’oÿ teil mesprison ! »
Dit li prestres : « Biax tres dolz sire,
toute parole se lait dire.
Mais je demant jor de conseil, 
qu’il est droiz que je me conseil
de ceste choze, c’il vos plait,
non pas que je i bee en plait.
- Je wel bien le conseil aiez, 
mais ne me tieng paz a paiez
de ceste choze c’ele est voire.
- Sire, ce ne fait pas a croire. »

Lors se part li vespques dou prestre,
qui ne tient pas le fait a feste. 
Li prestres ne s’esmaie mie 
qu’il seit bien qu’il at bone amie : 
c’est sa borce qui ne li faut
por amende ne por defaut.

Que que foz dort, et termes vient.
Li termes vint et cil revient.
.XX. livres en une corroie,
touz ses et de bone monoie,
aporta li prestres o soi. 
N’a garde qu’il ait fain ne soi !
Quant l’esvesques le voit venir, 
de parleir ne se pot tenir :
« Prestres, consoil aveiz eü,
qui aveiz votre senz beü.
- Sire, consoil oi ge cens faille,
mais a consoil n’afiert bataille.
Ne vos en deveiz mervillier,
qu’a consoil doit on concillier. 
Dire vos weul ma conscïence
et c’il i afiert penitance,
ou soit d’avoir ou soit de cors, 
adons si me corrigiez lors ! »

L’evesques si de li s’aprouche
que parleir i pout bouche a bouche,
et li prestres lieve la chiere,
qui lors n’out pas monoie chiere.
Desoz sa chape tint l’argent.
Ne l’ozat montreir pour la gent. 
En concillant conta son conte :
« Sire, ci n’afiert plus lonc conte.
Mes asnes at lonc tans vescu.
Mout avoie en li boen escu. 
Il m’at servi et volentiers
mout loiaument .xx. ans entiers.
Se je soie de Dieu assoux, 
chacun an gaiaingnoit .xx. soux 
tant qu’il at espairgnié .xx. livres ! 
Pour ce qu’il soit d’enfer delivres
les vos laisse en son testament. » 
Et dist l’esvesques : « Diex l’ament 
et si li pardoint ces meffais 
et toz les pechiez qu’il at fais ! »

Ensi con vos aveiz oÿ, 
dou riche prestre s’esjoÿ
l’evesquez por ce qu’il mesprit. 
A bontei faire li aprist ! 
Rutebués nos dist et enseigne 
qui deniers porte a sa besoingne
ne doit douteir mauvais lÿens.
Li asnes remest crestïens. 
A tant la rime vos en lais 
qu’il paiat bien et bel son lais ! 

Explicit 
