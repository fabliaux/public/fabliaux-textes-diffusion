Uns vilains ot sa femme espuse,
ki mult esteit contralïuse. 
Un jur furent ensemble alé 
pur eals dedure par un pré. 
Li vilains a sa femme ad dit 
quẹ unkes meis de ses oilz ne vit 
nul pré fauché si üelment. 
Elẹ li respunt hastivement :
« Einz fud od forces trenchez. » 
Dist li vilains : « Mes est fauchez !
- Mes est, fait la femme, tunduz ! »
Dunc est li vilains irascunz.
« Tu es, fait il, fole pruvee !
Cest herbe fut a falz copee, 
mes tu es engresse e fole 
q’avantveus mettre ta parole. 
La meiẹ vulés feire remaindre ;
par engresté me vulsateindre. »
Li vilains l’ad aval jettee, 
si li a sa lange colpee 
e puis demandẹ quẹ avis li fu 
quei ele en aveit entendu, 
se li prez fud od falz falchez 
u od forces fud trenchez.
La u ele ne pot parler, 
od ses deiz li prist a mustrer 
k’a forces l’aveient trenché 
e que falz nel ot pas seié.

Par cest essample vuil mustrer 
- bien le puet hoem suvent pruver - 
ke fols parole une folie 
e altrement qui le sens die,
nel en creit pas, einz s’en aïre.
La u il seit bien qu’il en est pire,
vult sa mençunge mettre avant.
Nul nel fereit de ceo taisant. 

