# Fabliaux Textes Diffusion


## FR
Cet entrepôt contient les textes des corpus publics du projet [Fabliaux](https://ciham.cnrs.fr/fabliaux/) aux différents formats. Sauf mention contraire dans le fichier, toutes les données sont diffusées sous la [Licence Ouverte Etalab](https://www.etalab.gouv.fr/licence-ouverte-open-licence/). Les répetoires suivants correspondent aux formats diffusés :

- TEI : Fichiers au format XML TEI P5. Voir l'[ODD](http://bfm.ens-lyon.fr/spip.php?article326) ;
- TEI-TXM : Fichiers au format XML TEI étendu pour la plateforme TXM. Tous les mots sont balisés &lt;tei:w&gt;, les propriétés de mots sont encodées dans les sous-éléments &lt;txm:ana&gt;. Voir la [Spécification](https://groupes.renater.fr/wiki/txm-info/public/xml_tei_txm) pour plus de détails ;
- TXT : Textes au format texte brut.


## EN

This repository contains the texts of the public corpora of the [Fabliaux Project](https://ciham.cnrs.fr/fabliaux/) in various formats. Unless otherwise stated in the file, all data are available under an [Etalab Open Licence](https://www.etalab.gouv.fr/licence-ouverte-open-licence/). The following directories correspond to the distributed formats:

- TEI: Files in XML TEI P5 format. See the [ODD](http://bfm.ens-lyon.fr/spip.php?article326) (in French);
- TEI-TXM: Files in TEI XML format extended for TXM Platform. All the words are tagged with &lt;tei:w&gt;, the word properties (annotations) are encoded as &lt;txm:ana&gt; sub-elements. See the [Specification](https://groupes.renater.fr/wiki/txm-info/public/xml_tei_txm) for more details.
- TXT: Simple text files.
